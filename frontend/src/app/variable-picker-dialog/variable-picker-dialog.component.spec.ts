import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VariablePickerDialogComponent } from './variable-picker-dialog.component';

describe('VariablePickerDialogComponent', () => {
  let component: VariablePickerDialogComponent;
  let fixture: ComponentFixture<VariablePickerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VariablePickerDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VariablePickerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
