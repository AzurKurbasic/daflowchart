import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export interface DialogData {
  name: string;
  type: string;
}

@Component({
  selector: 'app-variable-picker-dialog',
  templateUrl: './variable-picker-dialog.component.html',
  styleUrls: ['./variable-picker-dialog.component.scss']
})
export class VariablePickerDialogComponent{

  constructor(
    public dialogRef: MatDialogRef<VariablePickerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
