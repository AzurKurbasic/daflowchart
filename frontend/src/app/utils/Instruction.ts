import { FlowchartElement, FlowchartComponent } from './../flowchart/flowchart.component';
import { IElement } from './Element';


export interface IInstructionDB {
  id: string;
  elements: IElement[];
  next: IInstruction | null;
}

export interface IInstruction {
  id: string;
  elements: IElement[];
  next: IInstruction | null;
  previous: IInstruction | null;
  toString: () => string;
  toDBObject: () => IInstructionDB;

  flowchartElement?: FlowchartElement;

}

export class AssignmentDB implements IInstructionDB {
  constructor(public id: string,
    public elements: IElement[],
    public next: IInstruction | null
  ) {
  }
  // todo: implement toInstruction function when loading database object back
}

export class ConditionDB implements IInstructionDB {
  constructor(public id: string,
    public elements: IElement[],
    public next: IInstruction | null
  ) {
  }
  // todo: implement toInstruction function when loading database object back
}

export class Assignment implements IInstruction {
  flowchartElement?: FlowchartElement;

  constructor(
    public id: string,
    public elements: IElement[],
    public next: IInstruction | null,
    public previous: IInstruction | null) {

  }
  toString = () => {
    let instructionString = '';
    this.elements.map((element) => {
      instructionString += `${element.toString()}`;
    });
    return `${instructionString}`;
  }

  toDBObject(): IInstructionDB {
    return new AssignmentDB(
      this.id,
      this.elements,
      this.next
    );
  }
}

export class Condition implements IInstruction {
  next: IInstruction | null = null;
  constructor(
    public id: string,
    public elements: IElement[],
    public previous: IInstruction,
    public yes: IInstruction,
    public no: IInstruction
  ) {

  }

  toString = () => {
    let instructionString = '';
    this.elements.map((element) => {
      instructionString += `${element.toString()}`;
    });
    return instructionString;
  }

  toDBObject(): IInstructionDB {
    return new AssignmentDB(
      this.id,
      this.elements,
      this.next
    );
  }
  flowchartElement?: FlowchartElement;

}

export class Start implements IInstruction {
  id: string;
  elements: IElement[];
  next: null;
  previous: IInstruction | null;

  flowchartElement?: FlowchartElement;
  constructor(
  ) {
    this.id = 'START';
    this.elements = [];
    this.next = null;
    this.previous = null;
  }

  toDBObject: () => IInstructionDB;

  toString(): string {
    return ''
  }

}

export class End implements IInstruction {
  id: string;
  elements: IElement[];
  next: null;
  previous: IInstruction | null;
  flowchartElement?: FlowchartElement;

  constructor() {
    this.id = 'END';
    this.elements = [];
    this.next = null;
    this.previous = null;
  }

  toDBObject: () => IInstructionDB;

  toString (): string {
    return  ''
  }

}
