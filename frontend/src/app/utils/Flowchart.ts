import {Assignment, Condition, IInstruction, IInstructionDB, Start, End} from './Instruction';
import {
  ElementOperator,
  ElementPrimitive,
  ElementVariable,
  IElementOperatorTypes,
  IElementPrimitiveTypes
} from './Element';

export enum RunMode {
  DEBUG = 1,
  EXECUTION = 2
}

export class VariableUI {
  constructor(public name: string, public value: number| string| boolean| undefined) {
  }
}
export class Program {
  constructor(public instructions: IInstructionDB [], public variableStack: ElementVariable[]) {
  }
}

export class IFlowchart {

  vars: { [key: string]: number|string|boolean|undefined } = {};
  currentPosition: IInstruction;
  currentStatus: string;

  constructor(public name: string,
              public creator: string,
              public instructions: IInstruction [],
              public variableStack: ElementVariable []
              ) {
    this.variableStack.forEach(variable => this.initializeVariable(variable));
    this.currentStatus = 'Waiting for start';
  }

  run = (variableStack: ElementVariable []) => {

    this.variableStack = variableStack;
    const start = this.instructions[0];
    this.evaluate(start, RunMode.EXECUTION);
  }

  evaluate = (instruction: IInstruction, mode: RunMode) => {
    this.currentStatus = 'running';
    const evalString = instruction.toString();
    console.log("EVALUATION STRING", evalString)
    const vars = this.vars;
    if (instruction instanceof Condition) {
      // tslint:disable-next-line:no-eval
      const conditionResult = eval(evalString);
      console.log(conditionResult);
      if (conditionResult) {
        if (instruction.yes !== null && mode === RunMode.EXECUTION) {
          console.log('Condition fulfilled', instruction.yes);
          this.evaluate(instruction.yes, mode);
        }
        if (instruction.yes !== null && mode === RunMode.DEBUG) {
          console.log('Condition fulfilled', instruction.yes);
          this.currentPosition = instruction.yes;
          return;
        }
      }
      else {
        if (instruction.no !== null && mode === RunMode.EXECUTION) {
          console.log('Condition not fulfilled', instruction.no);
          this.evaluate(instruction.no, mode);
        }
        if (instruction.no !== null && mode === RunMode.DEBUG) {
          console.log('Condition not fulfilled', instruction.no);
          this.currentPosition = instruction.no;
          return;
        }
      }
    }
    else {
      // tslint:disable-next-line:no-eval
      eval(evalString);
      if (instruction.next !== null && mode === RunMode.EXECUTION) {
        console.log('Moving to next', instruction.next);
        this.evaluate(instruction.next, mode);
      }
      if (instruction.next !== null && mode === RunMode.DEBUG) {
        console.log('Moving to next', instruction.next);
        this.currentPosition = instruction.next;
        return;
      }
    }
    console.log('Ending program', instruction);
    this.currentStatus = 'ended';
  }

  initializeVariable = (variable: ElementVariable) => {
    this.vars[variable.name] = undefined;
  }

  stepIn = () => {
    if (this.currentPosition instanceof Start) {
      this.currentStatus = 'Started';
    }
    if (this.currentPosition instanceof End) {
      this.currentStatus = 'Ended';
      return
    }
    if (this.currentStatus !== 'ended') {
      const currentInstruction = !this.currentPosition ? this.instructions[0] : this.currentPosition;
      this.evaluate(currentInstruction, RunMode.DEBUG);
    }
  }

  getStatus = () => {
    return this.currentStatus;
  }

  getSnapshotUI(): VariableUI [] {
    const snapshot: VariableUI [] = [];
    for (const [variable, value] of Object.entries(this.vars)) {
      snapshot.push(
        new VariableUI(
          variable,
          value === undefined ? 'Nedefinirano' : value
        )
      );
    }
    return snapshot;
  }

  export(): Program {
    return new Program(
      this.instructions.map(item => item.toDBObject()),
      this.variableStack
    );
  }

}
