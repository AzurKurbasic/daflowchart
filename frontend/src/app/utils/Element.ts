export interface IElement {
    toString: () => string;
}

export class  ElementVariable implements IElement {
    constructor(public name: string, public type: IElementPrimitiveTypes = null) {}

    toString = () => {
        return `vars.${this.name}`;
    }
}

export class ElementOperator implements IElement {
    constructor(private type: IElementOperatorTypes) {}

    toString = () => {
        return `${this.type}`;
    }
}

export class ElementPrimitive {
    constructor(private type: IElementPrimitiveTypes, private value: string | number | boolean) {}

    toString = () => {
        return `${this.value}`;
    }
}


export enum IElementOperatorTypes {
    PLUS = '+',
    MINUS = '-',
    MULTIPLY = '*',
    DIVIDE = '/',
    PARENTHESES_OPEN = '(',
    PARENTHESES_CLOSE = ')',
    ASSIGN = '=',
    AND = '&&',
    OR = '||',
    EQUAL = '===',
    GREATER_THEN = '>',
    LESS_THEN = '<'
}

export enum IElementPrimitiveTypes {
    NUMBER = 0,
    STRING = 1,
    BOOLEAN = 2
}
