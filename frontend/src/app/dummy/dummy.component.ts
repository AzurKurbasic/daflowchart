import {Component, Injectable, Input, OnInit} from '@angular/core';
import {IFlowchart, VariableUI} from '../utils/Flowchart';
import {
  ElementOperator,
  ElementPrimitive,
  ElementVariable,
  IElementOperatorTypes,
  IElementPrimitiveTypes
} from '../utils/Element';
import {Assignment, Condition, IInstruction} from '../utils/Instruction';
import {Subject} from "rxjs";
import {DebuggerServiceService} from '../debugger-service.service';

@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.scss']
})
export class DummyComponent implements OnInit {

  variableStack: VariableUI [];
  flowChartInstance: IFlowchart;
  currentStatus: string;
  displayColumns: string [] = ['Variable name', 'Variable value'];

  constructor(private debuggerService: DebuggerServiceService) {
    debuggerService.sub.subscribe((event) => {
      console.log(event)
    });
    // this.loadProgram();
  }

  ngOnInit(): void { }

  stepIn(): void {
    if (this.currentStatus !== 'ended') {
      this.flowChartInstance.stepIn();
      this.variableStack = this.flowChartInstance.getSnapshotUI();
    }
    this.currentStatus = this.flowChartInstance.getStatus();
  }

  // loadProgram(): void {
  //   const varX = new ElementVariable('x');
  //
  //   const assign = new ElementOperator(IElementOperatorTypes.ASSIGN);
  //   const less = new ElementOperator(IElementOperatorTypes.LESS_THEN);
  //   const plus = new ElementOperator(IElementOperatorTypes.PLUS);
  //   const one = new ElementPrimitive(IElementPrimitiveTypes.NUMBER, 1);
  //   const two = new ElementPrimitive(IElementPrimitiveTypes.NUMBER, 2);
  //   const ten = new ElementPrimitive(IElementPrimitiveTypes.NUMBER, 10);
  //
  //   const firstInstruction = new Assignment('1', [varX, assign, one], null, null);
  //   const yesCondition = new Assignment('2',  [varX, assign, varX, plus, one], null, null);
  //   const secondInstruction = new Condition('3', [varX, less, ten], null, null, null);
  //   const endInstruction = new Assignment('4', [varX, one], null, null);
  //
  //   firstInstruction.next = secondInstruction;
  //   secondInstruction.yes = yesCondition;
  //   secondInstruction.no = null;
  //   yesCondition.next = secondInstruction;
  //
  //   const instructions: IInstruction [] = [firstInstruction, secondInstruction, yesCondition, endInstruction];
  //   const variableStack: string [] = ['x'];
  //
  //   this.flowChartInstance = new IFlowchart(
  //     'myChart',
  //     'creator',
  //     instructions,
  //     null
  //   );
  //   this.variableStack = this.flowChartInstance.getSnapshotUI();
  //   this.currentStatus = this.flowChartInstance.getStatus();
  // }

}
