import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  ElementOperator,
  ElementPrimitive,
  ElementVariable,
  IElement,
  IElementOperatorTypes,
  IElementPrimitiveTypes
} from "../utils/Element";

@Component({
  selector: 'app-instruction-input',
  templateUrl: './instruction-input.component.html',
  styleUrls: ['./instruction-input.component.scss']
})
export class InstructionInputComponent implements OnInit {

  constructor() { }

  instructionString: string;
  validString: boolean = false;
  currentExpression: IElement [] = [];

  @Input('variableStack')
  variableStack: ElementVariable[]

  @Output('evalString') evalString = new EventEmitter<string> ();

  @Output('expression') expression = new EventEmitter<IElement []> ();


  emitExpression() {
    this.buildExpression();
    console.log(this.currentExpression)
    this.evalString.emit(this.instructionString);
    this.expression.emit(this.currentExpression);
  }

  ngOnInit(): void {

  }

  buildExpression(): void {
    console.log("Checking if valid")
    const tokens = this.instructionString.split(' ');
    this.currentExpression = []
    tokens.forEach((token) => {
      let expr: IElement = null;
      switch (token) {
        case '+':
          expr = new ElementOperator(IElementOperatorTypes.PLUS)
          break
        case '-':
          expr = new ElementOperator(IElementOperatorTypes.MINUS)
          break
        case '*':
          expr = new ElementOperator(IElementOperatorTypes.MULTIPLY)
          break
        case '/':
          expr = new ElementOperator(IElementOperatorTypes.DIVIDE)
          break
        case '&&':
          expr = new ElementOperator(IElementOperatorTypes.AND)
          break
        case '||':
          expr = new ElementOperator(IElementOperatorTypes.OR)
          break
        case '<':
          expr = new ElementOperator(IElementOperatorTypes.LESS_THEN)
          break
        case '>':
          expr = new ElementOperator(IElementOperatorTypes.GREATER_THEN)
          break
        case '==':
          expr = new ElementOperator(IElementOperatorTypes.EQUAL)
          break;
        case '(':
          expr = new ElementOperator(IElementOperatorTypes.PARENTHESES_OPEN)
          break
        case ')':
          expr = new ElementOperator(IElementOperatorTypes.PARENTHESES_CLOSE)
          break
        case '=':
          expr = new ElementOperator(IElementOperatorTypes.ASSIGN)
      }
      if (expr == null && /^\d+$/.test(token)) {
        // tslint:disable-next-line:radix
        console.log(token, "token")
        console.log(typeof token)
        const number = (parseInt(token))
        expr = new ElementPrimitive(IElementPrimitiveTypes.NUMBER, number)
      }
      // It means it is variable
      if (expr == null) {
        let presentOnStack = false
        let variable = null
        this.variableStack.forEach((stackVariable) => {
          if (stackVariable.name === token) {
            presentOnStack = true
            variable = stackVariable
          }
        })
        if (presentOnStack) {
          expr = variable
        }
      }
      if (expr !== null) {
        this.currentExpression.push(expr)
      }
    })
  }
}
