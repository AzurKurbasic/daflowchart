import { WorkspaceComponent } from './workspace/workspace.component';
import { FlowchartComponent } from './flowchart/flowchart.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DummyComponent} from './dummy/dummy.component';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

const routes: Routes = [
  {
    path: 'workspace',
    component: WorkspaceComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), MatTableModule, MatIconModule, MatButtonModule],
  declarations: [
    DummyComponent
  ],
  exports: [RouterModule, DummyComponent]
})
export class AppRoutingModule { }
