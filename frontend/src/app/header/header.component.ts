import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import {DebuggerServiceService} from '../debugger-service.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  isUserLoggedIn: boolean = false;
  currentUrlView: string;
  debugger: boolean = false;
  constructor(
    private router: Router,
    private debuggerService: DebuggerServiceService
  ) { }

  ngOnInit() {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.handleRouteChange();
      }
    })
  }

  // callback when a route is changed
  handleRouteChange() {
    const urlPaths = this.router.routerState.snapshot.url.split('/').filter(p => p);
    this.currentUrlView = urlPaths[0];
  }

  // navigate to a specified route (called when any sidebar button is clicked)
  // navigates home if no route passed
  navigateTo(route: string) {
    if (route) {
      this.router.navigate(['', route]);
    } else {
      this.router.navigate(['']);
    }
  }

  // getter which returns whether a login button should be displayed anywhere
  get showLoginButton() {
    return (!this.isUserLoggedIn && this.currentUrlView !== 'login')
  }

  // getter which returns whether a home button should be displayed anywhere
  get showHomeButton() {
    return (this.currentUrlView)
  }

  // getter which returns whether a register button should be displayed anywhere
  get showRegisterButton() {
    return (!this.isUserLoggedIn && this.currentUrlView !== 'register');
  }

  toggleDebugger(): void {
    console.log('toggle');
    this.debugger = !this.debugger;
    this.debuggerService.sub.next(this.debugger);
  }

}
