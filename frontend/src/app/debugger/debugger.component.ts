import {Component, Input, OnInit} from '@angular/core';
import {
  ElementOperator,
  ElementPrimitive,
  ElementVariable,
  IElementOperatorTypes,
  IElementPrimitiveTypes
} from '../utils/Element';
import {Assignment, Condition, IInstruction} from "../utils/Instruction";
import {IFlowchart, VariableUI} from '../utils/Flowchart';
import {DebuggerServiceService} from "../debugger-service.service";

@Component({
  selector: 'app-debugger',
  templateUrl: './debugger.component.html',
  styleUrls: ['./debugger.component.scss']
})
export class DebuggerComponent implements OnInit {

  constructor() {

  }
  @Input('flowChartInstance')
  public flowChartInstance: IFlowchart;

  variableStack: VariableUI [];
  currentStatus: string;
  displayColumns: string [] = ['Variable name', 'Variable value'];
  ngOnInit(): void {
    console.log("DEBUGGER", this.flowChartInstance.instructions, this.flowChartInstance.variableStack)
  }


  stepIn(): void {
    if (this.flowChartInstance) {
      this.currentStatus = this.flowChartInstance.getStatus()
    }
    if (this.currentStatus !== 'ended') {
      this.flowChartInstance.stepIn();
      this.variableStack = this.flowChartInstance.getSnapshotUI();
    }
    this.currentStatus = this.flowChartInstance.getStatus();
  }
}
