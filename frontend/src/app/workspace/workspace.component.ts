import { FlowchartComponent } from './../flowchart/flowchart.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ElementVariable, IElementPrimitiveTypes } from '../utils/Element';
import { MatDialog } from '@angular/material/dialog';
import { VariablePickerDialogComponent } from '../variable-picker-dialog/variable-picker-dialog.component';
import { DialogData } from '../variable-picker-dialog/variable-picker-dialog.component';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss'],
})
export class WorkspaceComponent implements AfterViewInit {

  @ViewChild(MatSidenav, { static: false }) sidebar: MatSidenav;
  @ViewChild(FlowchartComponent, { static: false }) flowchartComponent: FlowchartComponent;

  variableStack: ElementVariable[] = [];

  ngAfterViewInit() {
    setTimeout(() => {
      this.flowchartComponent.addStart();
      this.flowchartComponent.addEnd();
    });
  }
  constructor(
    public dialog: MatDialog
  ) {
  }

  addToVariableStack(variableName: string, variableType: string): void {
    let varType: IElementPrimitiveTypes = null;
    switch (variableType) {
      case 'boolean':
        varType = IElementPrimitiveTypes.BOOLEAN;
        break;
      case 'number':
        varType = IElementPrimitiveTypes.NUMBER;
        break;
      case 'string':
        varType = IElementPrimitiveTypes.STRING;
        break;
    }

    this.variableStack.push(
      new ElementVariable(
        variableName,
        varType
      )
    );
  }

  popFromStack(element: ElementVariable): void {
    this.variableStack = this.variableStack.filter(item => item.name !== element.name);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(VariablePickerDialogComponent, {
      width: '250px',
      data: {
        name: '',
        type: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.name && result.type) {
        this.variableStack = this.variableStack.filter(item => item.name !== result.name);
        this.variableStack.push(
          new ElementVariable(
            result.name,
            result.type
          )
        );
      }
    });
  }

  onStepDropped($event: DragEvent) {
    console.log($event);
    if ($event.target instanceof Element) {
      if ($event.target.id == 'step-conditional') {
        this.flowchartComponent.addConditional($event.clientX, $event.clientY);
      } else if ($event.target.id == 'step-expression') {
        this.flowchartComponent.addStatement($event.clientX, $event.clientY);
      }
    }
  }

}
