import { TestBed } from '@angular/core/testing';

import { DebuggerServiceService } from './debugger-service.service';

describe('DebuggerServiceService', () => {
  let service: DebuggerServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DebuggerServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
