import { style } from '@angular/animations';
import { Assignment, End, IInstruction, Start, Condition } from './../utils/Instruction';
import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { IFlowchart } from '../utils/Flowchart';
import { DebuggerServiceService } from '../debugger-service.service';
import {
  IElement,
  ElementOperator,
  ElementPrimitive,
  ElementVariable,
  IElementOperatorTypes,
  IElementPrimitiveTypes
} from "../utils/Element";

const blockHeight: number = 80;
const blockWidth: number = 160;
const blockRadius: number = 5;
const circleRadius: number = blockHeight / 2;

@Component({
  selector: 'app-flowchart',
  templateUrl: './flowchart.component.html',
  styleUrls: ['./flowchart.component.scss']
})
export class FlowchartComponent implements OnInit, AfterViewInit {
  @ViewChild('canvas', { static: true }) canvasRef: ElementRef<HTMLCanvasElement>;
  canvas: any;
  canvasCtx: CanvasRenderingContext2D;
  showDebugger: boolean;

  constructor(debuggerService: DebuggerServiceService) {
    debuggerService.sub.subscribe((event) => {
      this.showDebugger = event as boolean;
    })
  }

  // **** ADDED ELEMENTS
  instructions: IInstruction[] = [];
  start: IInstruction;
  end: IInstruction;

  // UI
  @ViewChild('cursor', { static: false }) cursor: ElementRef;
  @ViewChild('cursorSelect', { static: false }) cursorSelect: ElementRef;
  currentHoverElement: IInstruction;
  selectedElement: IInstruction;
  connection: IInstruction[] = [];
  editingElement: IInstruction;
  flowChartInstance: IFlowchart;

  //FlowChart variables
  @Input('variableStack') variableStack: ElementVariable[];

  ngOnInit() {
    window.addEventListener('resize', this.redraw.bind(this));
    window.addEventListener('mousemove', this.handleMouseMove.bind(this));
    this.flowChartInstance = new IFlowchart(
      'my_chart',
      'danijel',
      this.instructions,
      this.variableStack
    )
  }

  ngAfterViewInit() {
    this.initRefs();
    this.drawGrid();
    this.redraw();
    this.setCursor('Normal');

    this.canvasRef.nativeElement.addEventListener('mousedown', this.handleCanvasMD.bind(this));
    this.canvasRef.nativeElement.addEventListener('mouseup', this.handleCanvasMU.bind(this));
    this.canvasRef.nativeElement.addEventListener('click', this.handleCanvasClick.bind(this));
    this.canvasRef.nativeElement.addEventListener('oncontextmenu', this.handleMenu.bind(this));
  }

  handleMouseMove($event: MouseEvent) {
    if (this.selectedElement) {
      this.selectedElement.flowchartElement.move($event.pageX, $event.pageY);
    }

    this.redraw();

    if (this.connection[0]) {
      this.drawConnectionTop(this.connection[0].flowchartElement.position, [$event.pageX, $event.pageY]);
    }

    this.moveCursor($event);
  }

  moveCursor($event: MouseEvent) {
    this.cursor.nativeElement.style.left = $event.pageX + 'px';
    this.cursor.nativeElement.style.top = $event.pageY + 'px';
  }

  setCursor(type: string) {
    switch (type) {
      case 'Normal': {
        this.cursor.nativeElement.style.visibility = 'visible';

        this.cursor.nativeElement.style.width = '12px';
        this.cursor.nativeElement.style.height = '12px';
        break;
      }

      case 'Select': {
        this.cursor.nativeElement.style.visibility = 'visible';

        this.cursor.nativeElement.style.width = '20px';
        this.cursor.nativeElement.style.height = '20px';
        break;
      }

      case 'None': {
        this.cursor.nativeElement.style.visibility = 'hidden';
        break;
      }
    }
  }

  handleCanvasMD() {
    if (this.currentHoverElement) {
      this.selectedElement = this.currentHoverElement;
    } else {
      this.selectedElement = null;
      this.connection = [];
    }
  }

  handleCanvasMU() {
    if (this.selectedElement) {
      this.selectedElement = null;
    }
  }

  handleCanvasClick($event: MouseEvent) {
    if (!this.connection[0]) {
      this.connection[0] = this.currentHoverElement;
    } else {
      if (this.connection[0] !== this.currentHoverElement) {
        this.connection[1] = this.currentHoverElement;
      }
    }

    if (this.connection[0] === this.connection[1]) {
      this.connection = [];
      return;
    }

    if (this.connection[0] && this.connection[1]) {
      this.connect(this.connection[0], this.connection[1]);
      this.connection = [];
    }
  }

  // This add  connection
  connect(from: IInstruction, to: IInstruction) {
    if (from instanceof Condition) {
      if (!from.yes) {
        from.yes = to
      }
      else if (!from.no) {
        from.no = to
      }
    }
    from.next = to;
    to.previous = from;
    this.redraw();
  }

  redraw() {
    this.drawGrid();

    for (const i of this.instructions) {
      const selected = i === this.editingElement;
      console.log(selected, i)
      if ((i instanceof Start) || (i instanceof End)) {
        const text = i instanceof Start ? 'start' : 'end';
        this.drawCircle(i.flowchartElement.position[0], i.flowchartElement.position[1], text, selected);
      } else if (i instanceof Assignment) {
        this.drawStatement(i.flowchartElement.position[0], i.flowchartElement.position[1], selected, i.flowchartElement.text);
      } else if (i instanceof Condition) {
        this.drawConditional(i.flowchartElement.position[0], i.flowchartElement.position[1], selected, i.flowchartElement.text);
      }

      if (i.next) {
        // this.drawConnection(i.flowchartElement.position, i.next.flowchartElement.position);
        this.drawConnection(i.flowchartElement.position, i.next.flowchartElement.position);
      }
      if (i instanceof Condition) {
        if (i.yes) {
          this.drawConnection(i.flowchartElement.position, i.yes.flowchartElement.position)
        }
        if (i.no) {
          this.drawConnection(i.flowchartElement.position, i.no.flowchartElement.position)
        }
      }
    }
  }

  initRefs() {
    this.canvas = this.canvasRef.nativeElement;
    this.canvasCtx = this.canvas.getContext('2d');
    this.canvas.width = this.canvas.getBoundingClientRect().width;
    this.canvas.height = this.canvas.getBoundingClientRect().height;

    this.canvasRef.nativeElement.addEventListener('mousemove', this.checkElementHovers.bind(this));
  }

  drawGrid() {
    this.canvasCtx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.canvasCtx.lineWidth = 0.5;
    this.canvasCtx.strokeStyle = '#000000';
    const step = (this.canvas.width / 35);

    for (let i = 0; i < step; i++) {
      this.canvasCtx.beginPath();
      this.canvasCtx.moveTo(i * step, 0);
      this.canvasCtx.lineTo(i * step, this.canvas.height);
      this.canvasCtx.stroke();
      this.canvasCtx.closePath();
    }

    for (let i = 0; i < step; i++) {
      this.canvasCtx.beginPath();
      this.canvasCtx.moveTo(0, i * step * 1.2);
      this.canvasCtx.lineTo(this.canvas.width, i * step * 1.2);
      this.canvasCtx.stroke();
      this.canvasCtx.closePath();
    }

    this.canvasCtx.strokeStyle = '#000000';
    this.canvasCtx.lineWidth = 2;
  }
  //Adds new statetement
  addStatement(x: number, y: number) {
    const statementInstruction: Assignment = new Assignment(`INSTRUCTION_${this.instructions.length}`, [], null, this.start);
    statementInstruction.flowchartElement = this.drawStatement(x, y);
    this.instructions.push(statementInstruction);
  }

  //Adds  nec ondition
  addConditional(x: number, y: number) {
    const statementConditional: Condition = new Condition(`INSTRUCTION_${this.instructions.length}`, [], null, null, null);
    statementConditional.flowchartElement = this.drawConditional(x, y);
    this.instructions.push(statementConditional);
  }

  drawStatement(x: number, y: number, selected: boolean = false, text: string = 'none'): FlowchartElement {
    this.canvasCtx.beginPath();
    this.canvasCtx.font = '15px arial';
    this.canvasCtx.moveTo(x - blockWidth / 2, y - blockHeight / 2);
    this.canvasCtx.lineTo(x + blockWidth / 2, y - blockHeight / 2);

    this.canvasCtx.lineTo(x + blockWidth / 2, y + blockHeight / 2);

    this.canvasCtx.lineTo(x - blockWidth / 2, y + blockHeight / 2);

    this.canvasCtx.lineTo(x - blockWidth / 2, y - blockHeight / 2);

    this.canvasCtx.closePath();
    this.canvasCtx.fillStyle = selected ? 'red' : 'white';
    this.canvasCtx.fill();
    this.canvasCtx.fillStyle = 'black';
    this.canvasCtx.fillText(text, x - 30, y);

    return new FlowchartElement([x, y]);
  }

  drawConditional(x: number, y: number, selected: boolean = false, text: string = 'none') {
    let blockH = 120;
    let blockW = 120;
    // this.canvasCtx.beginPath();  // TODO remove
    // this.canvasCtx.moveTo(x, y + (blockH / 2));
    // this.canvasCtx.lineTo(x + (blockW / 2), y + blockH);
    // this.canvasCtx.lineTo(x + blockW, y + (blockH / 2));
    // this.canvasCtx.lineTo(x + (blockW / 2), y);
    // this.canvasCtx.lineTo(x, y + (blockH / 2));
    // this.canvasCtx.closePath();
    // this.canvasCtx.fillStyle = 'white';
    // this.canvasCtx.fill();
    this.canvasCtx.beginPath();
    this.canvasCtx.font = '15px arial';
    this.canvasCtx.moveTo(x, y + (blockH / 2));
    this.canvasCtx.lineTo(x + (blockW / 2), y);
    this.canvasCtx.lineTo(x, y - (blockH / 2));
    this.canvasCtx.lineTo(x - (blockW / 2), y);
    this.canvasCtx.moveTo(x - (blockW / 2), y);
    this.canvasCtx.lineTo(x, y - (blockH / 2));
    this.canvasCtx.closePath();
    this.canvasCtx.fillStyle = selected ? 'red' : 'white';
    this.canvasCtx.stroke()
    this.canvasCtx.fill();
    this.canvasCtx.fillStyle = 'black';
    this.canvasCtx.fillText(text, x, y);

    return new FlowchartElement([x, y]);
  }

  //Narise ga in doda ga a pod array instructionov
  addStart() {
    const startInstruction: Start = new Start();
    this.instructions.push(startInstruction);
    startInstruction.flowchartElement = this.drawCircle(this.canvas.width / 2, 100, 'start');


    this.start = startInstruction;
  }

  //  Narse ga in ga  doad na konce  array instrucitonov
  addEnd() {
    const endInstruction: End = new End();
    this.instructions.push(endInstruction);
    endInstruction.flowchartElement = this.drawCircle(this.canvas.width / 2, this.canvas.height - 100, 'end' );
    this.end = endInstruction;
  }

  drawCircle(x: number, y: number, text: string, selected: boolean = false): FlowchartElement {
    this.canvasCtx.beginPath();
    this.canvasCtx.font = '15px arial';
    this.canvasCtx.arc(x, y, circleRadius, 0, 2 * Math.PI, false);
    this.canvasCtx.fillStyle = '#303030';
    this.canvasCtx.closePath();
    // this.canvasCtx.fill();
    this.canvasCtx.lineWidth = 5;
    this.canvasCtx.strokeStyle = selected ? 'red' : 'white';
    this.canvasCtx.stroke();
    this.canvasCtx.fillStyle = selected ? 'red' : 'white';
    this.canvasCtx.fillText(text, x - 30, y);

    return new FlowchartElement([x, y]);
  }

  checkElementHovers($event: MouseEvent) {
    let isHovered = false;

    for (const i of this.instructions) {
      if (i.flowchartElement) {
        if (i.flowchartElement.detectHover($event.pageX, $event.pageY)) {
          isHovered = true;
          this.currentHoverElement = i;
          this.setCursor('Select');
        }
      }
    }

    if (!isHovered && !this.selectedElement) {
      this.currentHoverElement = null;
      this.setCursor('Normal');
    }
  }

  onCanvasMouseOn() {
    this.setCursor('Normal');
  }

  onCanvasMouseOut() {
    this.setCursor('None');
  }

  drawConnection(from: number[], to: number[]) {
    if (from[0] > to[0]) {
      this.drawConnectionTop(from, to);
    } else {
      this.drawConnectionSide(from, to);
    }
  }

  drawConnectionTop(from: number[], to: number[]) {
    // line color
    this.canvasCtx.strokeStyle = "red";

    // line drawing
    this.canvasCtx.lineWidth = 2;
    this.canvasCtx.lineCap = 'round';

    this.canvasCtx.beginPath();
    this.canvasCtx.moveTo(from[0], from[1]);
    this.canvasCtx.lineTo(to[0], from[1]);

    // arrow params
    let x_size = 4;
    let y_size = 10;

    if (to[0] < from[0]) {
      x_size = x_size * -1;
    }

    if (from[1] > to[1]) {
      y_size = y_size * -1;
    }

    this.canvasCtx.lineTo(to[0], to[1] - y_size);     // TODO handli za predznake
    this.canvasCtx.moveTo(to[0], to[1] - y_size);
    this.canvasCtx.closePath();
    this.canvasCtx.stroke();

    // arrow
    this.canvasCtx.beginPath();
    this.canvasCtx.moveTo(to[0], to[1] - y_size);
    this.canvasCtx.lineTo(to[0] + x_size, to[1] - y_size);
    this.canvasCtx.lineTo(to[0], to[1]);
    this.canvasCtx.moveTo(to[0], to[1]);
    this.canvasCtx.lineTo(to[0] - x_size, to[1] - y_size);
    this.canvasCtx.lineTo(to[0], to[1] - y_size);
    this.canvasCtx.moveTo(to[0], to[1] - y_size);

    this.canvasCtx.closePath();
    // arrow color
    this.canvasCtx.strokeStyle = "red";
    this.canvasCtx.fillStyle = "red";

    this.canvasCtx.fill();
    this.canvasCtx.stroke();
  }

  drawConnectionSide(from: number[], to: number[]) {
    // line color
    this.canvasCtx.strokeStyle = "red";

    // line drawing
    this.canvasCtx.lineWidth = 2;
    this.canvasCtx.lineCap = "round";

    this.canvasCtx.beginPath();
    this.canvasCtx.moveTo(from[0], from[1]);
    this.canvasCtx.lineTo(from[0], to[1]);

    // arrow params
    let x_size = 10;
    let y_size = 4;

    if (to[0] < from[0]) {
      x_size = x_size * -1;
    }

    if (from[1] > to[1]) {
      y_size = y_size * -1;
    }

    this.canvasCtx.lineTo(to[0] - x_size, to[1]);     // TODO handli za predznake
    this.canvasCtx.moveTo(to[0] - x_size, to[1]);
    this.canvasCtx.closePath();
    this.canvasCtx.stroke();

    // arrow
    this.canvasCtx.beginPath();
    this.canvasCtx.moveTo(to[0] - x_size, to[1]);
    this.canvasCtx.lineTo(to[0] - x_size, to[1] - y_size);
    this.canvasCtx.lineTo(to[0], to[1]);
    this.canvasCtx.moveTo(to[0], to[1]);
    this.canvasCtx.lineTo(to[0] - x_size, to[1] + y_size);
    this.canvasCtx.lineTo(to[0] - x_size, to[1]);
    this.canvasCtx.moveTo(to[0] - x_size, to[1]);

    this.canvasCtx.closePath();
    // arrow color
    this.canvasCtx.strokeStyle = "red";
    this.canvasCtx.fillStyle = "red";
    this.canvasCtx.stroke();
    this.canvasCtx.fill();
  }

  handleMenu($event: MouseEvent) {
    $event.preventDefault()
    let isHovered = false;
    let currentElement = null;

    for (const i of this.instructions) {
      if (i.flowchartElement) {
        if (i.flowchartElement.detectHover($event.pageX, $event.pageY)) {
          isHovered = true;
          currentElement = i;
        }
      }
    }

    if (!isHovered && !currentElement) {
      this.editingElement = null;
      this.setCursor('Normal');
    }else{
      this.editingElement = currentElement;
    }
  }

  //emitted expression
  setExpression(expresion: IElement []) {
    console.log("SET EXPRESSION !!!", expresion)
    console.log(this.instructions, "INSTRUCTIONS")
    this.editingElement.elements = expresion;
  }

  //emitted string
  drawString(text: string) {
    console.log("DRAW STRING !!!!", text)
    this.editingElement.flowchartElement.text = text;
  }
}

export class FlowchartElement {
  public bounds: number[][];

  public text: string ='None';

  constructor(
    public position: number[]) {
    this.calculateBounds();
  }

  detectHover: (positionX: number, positionY: number) => boolean = (positionX: number, positionY: number) => {
    if (
      (positionX >= this.bounds[0][0] && positionX >= this.bounds[2][0])
      &&
      (positionX <= this.bounds[1][0] && positionX <= this.bounds[3][0])
      &&
      (positionY >= this.bounds[0][1] && positionY >= this.bounds[1][1])
      &&
      (positionY <= this.bounds[2][1] && positionY <= this.bounds[3][1])
    ) {
      return true;
    }
  }

  move(x: number, y: number) {
    this.position = [x, y];
    this.calculateBounds();
  }

  calculateBounds() {
    this.bounds = [[this.position[0] - blockWidth / 2, this.position[1] - blockHeight / 2], [this.position[0] + blockWidth / 2, this.position[1] - blockHeight / 2], [this.position[0] - blockWidth / 2, this.position[1] + blockHeight / 2], [this.position[0] + blockWidth / 2, this.position[1] + blockHeight / 2]];
  }
}
