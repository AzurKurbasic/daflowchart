import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FlowchartComponent } from './flowchart/flowchart.component';

import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { WorkspaceComponent } from './workspace/workspace.component';
import {HeaderComponent} from './header/header.component';
import { VariablePickerDialogComponent } from './variable-picker-dialog/variable-picker-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule} from '@angular/forms';
import { DebuggerComponent } from './debugger/debugger.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { InstructionInputComponent } from './instruction-input/instruction-input.component';

@NgModule({
  declarations: [
    AppComponent,
    FlowchartComponent,
    WorkspaceComponent,
    HeaderComponent,
    VariablePickerDialogComponent,
    DebuggerComponent,
    InstructionInputComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatSelectModule,
    MatCardModule,
    MatInputModule,
    MatTableModule,
    MatDialogModule,
    FormsModule,
    MatSlideToggleModule
  ],
  providers: [],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
